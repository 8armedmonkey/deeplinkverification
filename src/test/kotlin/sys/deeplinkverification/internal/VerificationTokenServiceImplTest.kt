package sys.deeplinkverification.internal

import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import sys.deeplinkverification.domain.VerificationTokenService
import java.util.concurrent.TimeUnit

class VerificationTokenServiceImplTest {

    private lateinit var verificationTokenService: VerificationTokenService

    @Before
    fun setUp() {
        verificationTokenService = VerificationTokenServiceImpl(TimeUnit.HOURS.toMillis(1))
    }

    @Test
    fun whenGenerateNewTokenThenItShouldGenerateNewToken() {
        val verificationToken = verificationTokenService.generateNewToken()

        assertTrue(verificationToken.token.isNotEmpty())
        assertTrue(verificationToken.expiryTimeMillis > System.currentTimeMillis())
    }

}