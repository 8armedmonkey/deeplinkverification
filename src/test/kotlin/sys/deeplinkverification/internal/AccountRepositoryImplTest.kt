package sys.deeplinkverification.internal

import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import sys.deeplinkverification.domain.Account
import sys.deeplinkverification.domain.AccountRepository
import sys.deeplinkverification.domain.EmailAddress
import sys.deeplinkverification.domain.VerificationToken
import sys.deeplinkverification.infrastructure.database.*
import java.util.*
import java.util.concurrent.TimeUnit

class AccountRepositoryImplTest {

    private lateinit var databaseConnectionPool: DatabaseConnectionPool
    private lateinit var accountRepository: AccountRepository

    @Before
    fun setUp() {
        databaseConnectionPool = DatabaseConnectionPoolImpl
        databaseConnectionPool.getConnection().use { it.dropDatabase() }

        accountRepository = AccountRepositoryImpl(databaseConnectionPool)

        migrateDatabase(databaseConnectionPool.getDataSource())
    }

    @Test
    fun whenRetrieveAndAccountExistsThenItShouldReturnTheAccount() {
        val account = Account(
            emailAddress = EmailAddress("foo@bar.baz"),
            passwordHash = "secret",
            verificationToken = VerificationToken(
                token = "token",
                expiryTimeMillis = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(5)
            )
        )

        accountRepository.store(account)

        val storedAccount = accountRepository.retrieve(account.id)

        assertEquals(account.id, storedAccount?.id)
        assertEquals(account.emailAddress, storedAccount?.emailAddress)
        assertEquals(account.passwordHash, storedAccount?.passwordHash)
        assertEquals(account.verificationToken, storedAccount?.verificationToken)
    }

    @Test
    fun whenRetrieveAndAccountNotExistsThenItShouldReturnNull() {
        val storedAccount = accountRepository.retrieve(UUID.randomUUID())

        assertNull(storedAccount)
    }

    @Test
    fun whenRetrieveByEmailAddressAndAccountExistsThenItShouldReturnTheAccount() {
        val account = Account(
            emailAddress = EmailAddress("foo@bar.baz"),
            passwordHash = "secret",
            verificationToken = VerificationToken(
                token = "token",
                expiryTimeMillis = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(5)
            )
        )

        accountRepository.store(account)

        val storedAccount = accountRepository.retrieveByEmailAddress(account.emailAddress)

        assertEquals(account.id, storedAccount?.id)
        assertEquals(account.emailAddress, storedAccount?.emailAddress)
        assertEquals(account.passwordHash, storedAccount?.passwordHash)
        assertEquals(account.verificationToken, storedAccount?.verificationToken)
    }

    @Test
    fun whenRetrieveByEmailAddressAndAccountNotExistsThenItShouldReturnNull() {
        val storedAccount = accountRepository.retrieveByEmailAddress(EmailAddress("oof@rab.zab"))

        assertNull(storedAccount)
    }

    @Test
    fun whenRetrieveByTokenAndAccountExistsThenItShouldReturnTheAccount() {
        val account = Account(
            emailAddress = EmailAddress("foo@bar.baz"),
            passwordHash = "secret",
            verificationToken = VerificationToken(
                token = "token",
                expiryTimeMillis = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(5)
            )
        )

        accountRepository.store(account)

        val storedAccount = accountRepository.retrieveByToken("token")

        assertEquals(account.id, storedAccount?.id)
        assertEquals(account.emailAddress, storedAccount?.emailAddress)
        assertEquals(account.passwordHash, storedAccount?.passwordHash)
        assertEquals(account.verificationToken, storedAccount?.verificationToken)
    }

    @Test
    fun whenRetrieveByTokenAndAccountNotExistsThenItShouldReturnNull() {
        val storedAccount = accountRepository.retrieveByToken("test")

        assertNull(storedAccount)
    }

    @Test
    fun whenCheckIfContainsAndAccountExistsThenItShouldReturnTrue() {
        val account = Account(
            emailAddress = EmailAddress("foo@bar.baz"),
            passwordHash = "secret",
            verificationToken = VerificationToken(
                token = "token",
                expiryTimeMillis = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(5)
            )
        )

        accountRepository.store(account)

        val contains = accountRepository.contains(account.id)

        assertTrue(contains)
    }

    @Test
    fun whenCheckIfContainsAndAccountNotExistsThenItShouldReturnFalse() {
        val contains = accountRepository.contains(UUID.randomUUID())

        assertFalse(contains)
    }

    @Test
    fun whenCheckIfContainsEmailAddressAndAccountExistsThenItShouldReturnTrue() {
        val account = Account(
            emailAddress = EmailAddress("foo@bar.baz"),
            passwordHash = "secret",
            verificationToken = VerificationToken(
                token = "token",
                expiryTimeMillis = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(5)
            )
        )

        accountRepository.store(account)

        val contains = accountRepository.containsEmailAddress(account.emailAddress)

        assertTrue(contains)
    }

    @Test
    fun whenCheckIfContainsEmailAddressAndAccountNotExistsThenItShouldReturnFalse() {
        val contains = accountRepository.containsEmailAddress(EmailAddress("oof@rab.zab"))

        assertFalse(contains)
    }

    @Test
    fun whenCheckIfContainsTokenAndAccountExistsThenItShouldReturnTrue() {
        val account = Account(
            emailAddress = EmailAddress("foo@bar.baz"),
            passwordHash = "secret",
            verificationToken = VerificationToken(
                token = "token",
                expiryTimeMillis = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(5)
            )
        )

        accountRepository.store(account)

        val contains = accountRepository.containsToken("token")

        assertTrue(contains)
    }

    @Test
    fun whenCheckIfContainsTokenAndAccountNotExistsThenItShouldReturnFalse() {
        val contains = accountRepository.containsToken("test")

        assertFalse(contains)
    }

    @After
    fun tearDown() {
        databaseConnectionPool.getConnection().use { it.dropDatabase() }
    }

}