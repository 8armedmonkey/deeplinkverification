package sys.deeplinkverification.internal

import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import sys.deeplinkverification.domain.Password
import sys.deeplinkverification.domain.PasswordHashingService

class PasswordHashingServiceImplTest {

    private lateinit var passwordHashingService: PasswordHashingService

    @Before
    fun setUp() {
        passwordHashingService = PasswordHashingServiceImpl()
    }

    @Test
    fun whenHashThenSameInputShouldBeIdentifiedAsMatch() {
        val password = Password("12345678")
        val hash = passwordHashingService.hash(password)

        assertTrue(passwordHashingService.check(password, hash))
    }

    @Test
    fun whenHashThenDifferentInputShouldBeIdentifiedAsMismatched() {
        val password1 = Password("12345678")
        val password2 = Password("87654321")
        val hash = passwordHashingService.hash(password1)

        assertFalse(passwordHashingService.check(password2, hash))
    }

}