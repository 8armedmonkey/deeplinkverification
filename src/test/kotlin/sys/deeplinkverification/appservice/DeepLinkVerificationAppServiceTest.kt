package sys.deeplinkverification.appservice

import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import sys.deeplinkverification.domain.*
import java.net.URI
import java.util.*
import java.util.concurrent.TimeUnit

class DeepLinkVerificationAppServiceTest {

    private lateinit var accountRepository: AccountRepository
    private lateinit var passwordHashingService: PasswordHashingService
    private lateinit var deepLinkService: DeepLinkService
    private lateinit var deepLinkSendingService: DeepLinkSendingService
    private lateinit var verificationTokenService: VerificationTokenService
    private lateinit var deepLinkVerificationAppService: DeepLinkVerificationAppService

    @Before
    fun setUp() {
        accountRepository = mock()
        passwordHashingService = mock()
        deepLinkService = DeepLinkService(URI.create("foo://bar/baz"))
        deepLinkSendingService = mock()
        verificationTokenService = mock()
        deepLinkVerificationAppService = DeepLinkVerificationAppService(
            accountRepository = accountRepository,
            passwordHashingService = passwordHashingService,
            deepLinkService = deepLinkService,
            deepLinkSendingService = deepLinkSendingService,
            verificationTokenService = verificationTokenService
        )
    }

    @Test
    fun whenRegisterAndAccountNotExistsThenItShouldCreateNewAccount() {
        val emailAddress = EmailAddress("foo@bar.baz")
        val password = Password("password")
        val passwordHash = "secret"

        FakeAccountRepository.setUpAccountNotExists(
            accountRepository,
            emailAddress
        )

        FakePasswordHashingService.setUpHash(
            passwordHashingService,
            password,
            passwordHash
        )

        deepLinkVerificationAppService.register(
            emailAddress.value,
            password.value
        )

        argumentCaptor<Account>().apply {
            verify(accountRepository).store(capture())

            assertEquals(emailAddress, firstValue.emailAddress)
            assertEquals(passwordHash, firstValue.passwordHash)
            assertNull(firstValue.verificationToken)
        }
    }

    @Test(expected = AccountAlreadyExistsException::class)
    fun whenRegisterAndAccountExistsThenItShouldThrowException() {
        val emailAddress = EmailAddress("foo@bar.baz")
        val password = Password("password")
        val passwordHash = "secret"

        val account = Account(
            id = UUID.randomUUID(),
            emailAddress = emailAddress,
            passwordHash = passwordHash
        )

        FakeAccountRepository.setUpAccountExists(
            accountRepository,
            account
        )

        deepLinkVerificationAppService.register(
            emailAddress.value,
            password.value
        )
    }

    @Test(expected = InvalidEmailAddressException::class)
    fun whenRegisterAndEmailNotValidThenItShouldThrowException() {
        deepLinkVerificationAppService.register("foo", "password")
    }

    @Test(expected = InsufficientPasswordLengthException::class)
    fun whenRegisterAndPasswordNotValidThenItShouldThrowException() {
        deepLinkVerificationAppService.register("foo@bar.baz", "pass")
    }

    @Test
    fun whenRequestNewVerificationThenItShouldAssignNewVerificationToken() {
        val account = Account(
            emailAddress = EmailAddress("foo@bar.baz"),
            passwordHash = "secret"
        )

        val newVerificationToken = VerificationToken(
            token = "newToken",
            expiryTimeMillis = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(5)
        )

        FakeAccountRepository.setUpAccountExists(
            accountRepository,
            account
        )

        FakeVerificationTokenService.setUpGenerateNewToken(
            verificationTokenService,
            newVerificationToken
        )

        deepLinkVerificationAppService.requestNewVerification(account.id)

        verify(verificationTokenService).generateNewToken()

        argumentCaptor<Account>().apply {
            verify(accountRepository).store(capture())

            assertEquals(newVerificationToken, firstValue.verificationToken)
        }
    }

    @Test
    fun whenRequestNewVerificationThenItShouldSendDeepLinkToEmailAddress() {
        val account = Account(
            emailAddress = EmailAddress("foo@bar.baz"),
            passwordHash = "secret"
        )

        val newVerificationToken = VerificationToken(
            token = "newToken",
            expiryTimeMillis = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(5)
        )

        FakeAccountRepository.setUpAccountExists(
            accountRepository,
            account
        )

        FakeVerificationTokenService.setUpGenerateNewToken(
            verificationTokenService,
            newVerificationToken
        )

        deepLinkVerificationAppService.requestNewVerification(account.id)

        val emailAddressArgumentCaptor = argumentCaptor<EmailAddress>()
        val deepLinkArgumentCaptor = argumentCaptor<URI>()

        verify(deepLinkSendingService).sendDeepLink(
            emailAddressArgumentCaptor.capture(),
            deepLinkArgumentCaptor.capture()
        )

        assertEquals(
            account.emailAddress,
            emailAddressArgumentCaptor.firstValue
        )

        assertEquals(
            "foo://bar/baz?token=newToken",
            deepLinkArgumentCaptor.firstValue.toString()
        )
    }

    @Test(expected = AccountNotExistsException::class)
    fun whenRequestNewVerificationAndAccountNotExistsThenItShouldThrowException() {
        val accountId = UUID.randomUUID()

        FakeAccountRepository.setUpAccountNotExists(
            accountRepository,
            accountId
        )

        deepLinkVerificationAppService.requestNewVerification(accountId)
    }

    @Test(expected = AccountNotExistsException::class)
    fun whenRequestNewVerificationByEmailAddressAndAccountNotExistsThenItShouldThrowException() {
        val emailAddress = EmailAddress("foo@bar.baz")

        FakeAccountRepository.setUpAccountNotExists(
            accountRepository,
            emailAddress
        )

        deepLinkVerificationAppService.requestNewVerification(emailAddress.value)
    }

    @Test
    fun whenVerifyTokenAndTokenIsValidThenItShouldReturnValid() {
        val token = "token"

        val account = Account(
            emailAddress = EmailAddress("foo@bar.baz"),
            passwordHash = "secret",
            verificationToken = VerificationToken(
                token = token,
                expiryTimeMillis = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(5)
            )
        )

        FakeAccountRepository.setUpAccountExists(
            accountRepository,
            account
        )

        val verificationStatus = deepLinkVerificationAppService.verifyToken(
            token,
            System.currentTimeMillis()
        )

        assertTrue(verificationStatus is VerificationStatus.Valid)
    }

    @Test
    fun whenVerifyTokenAndTokenNotExistsThenItShouldReturnInvalid() {
        val token = "token"

        FakeAccountRepository.setUpAccountNotExists(
            accountRepository,
            token
        )

        val verificationStatus = deepLinkVerificationAppService.verifyToken(
            token,
            System.currentTimeMillis()
        )

        assertTrue(verificationStatus is VerificationStatus.Invalid)
        assertTrue(
            (verificationStatus as VerificationStatus.Invalid).reason
                    is TokenVerificationInvalidReason.NotExists
        )
    }

    @Test
    fun whenVerifyTokenAndTokenIsExpiredThenItShouldReturnInvalid() {
        val token = "token"

        val account = Account(
            emailAddress = EmailAddress("foo@bar.baz"),
            passwordHash = "secret",
            verificationToken = VerificationToken(
                token = token,
                expiryTimeMillis = System.currentTimeMillis() - TimeUnit.MINUTES.toMillis(5)
            )
        )

        FakeAccountRepository.setUpAccountExists(
            accountRepository,
            account
        )

        val verificationStatus = deepLinkVerificationAppService.verifyToken(
            token,
            System.currentTimeMillis()
        )

        assertTrue(verificationStatus is VerificationStatus.Invalid)
        assertTrue(
            (verificationStatus as VerificationStatus.Invalid).reason
                    is TokenVerificationInvalidReason.Expired
        )
    }

    @Test
    fun whenIdentifyUserWithTokenAndPasswordWithValidTokenAndPasswordThenItShouldReturnValid() {
        val token = "token"
        val password = Password("password")
        val passwordHash = "secret"

        val account = Account(
            emailAddress = EmailAddress("foo@bar.baz"),
            passwordHash = passwordHash,
            verificationToken = VerificationToken(
                token = token,
                expiryTimeMillis = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(5)
            )
        )

        FakeAccountRepository.setUpAccountExists(
            accountRepository,
            account
        )

        FakePasswordHashingService.setUpHash(
            passwordHashingService,
            password,
            passwordHash
        )

        val identificationStatus = deepLinkVerificationAppService.identifyUserWithTokenAndPassword(
            token,
            password.value,
            System.currentTimeMillis()
        )

        assertTrue(identificationStatus is IdentificationStatus.Identified)
        assertEquals(
            account.id,
            (identificationStatus as IdentificationStatus.Identified).accountId
        )
    }

    @Test
    fun whenTokenIsAlreadyUsedForSuccessfulIdentificationThenItShouldNotBeReusable() {
        val token = "token"
        val password = Password("password")
        val passwordHash = "secret"

        val account = Account(
            emailAddress = EmailAddress("foo@bar.baz"),
            passwordHash = passwordHash,
            verificationToken = VerificationToken(
                token = token,
                expiryTimeMillis = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(5)
            )
        )

        FakeAccountRepository.setUpAccountExists(
            accountRepository,
            account
        )

        FakePasswordHashingService.setUpHash(
            passwordHashingService,
            password,
            passwordHash
        )

        deepLinkVerificationAppService.identifyUserWithTokenAndPassword(
            token,
            password.value,
            System.currentTimeMillis()
        )

        argumentCaptor<Account>().apply {
            verify(accountRepository).store(capture())

            assertNull(firstValue.verificationToken)
        }
    }

    @Test
    fun whenIdentifyUserWithTokenAndPasswordWithInvalidTokenThenItShouldReturnUnidentified() {
        val token = "token"
        val password = Password("password")
        val passwordHash = "secret"

        FakeAccountRepository.setUpAccountNotExists(
            accountRepository,
            token
        )

        FakePasswordHashingService.setUpHash(
            passwordHashingService,
            password,
            passwordHash
        )

        val identificationStatus = deepLinkVerificationAppService.identifyUserWithTokenAndPassword(
            token,
            password.value,
            System.currentTimeMillis()
        )

        assertTrue(identificationStatus is IdentificationStatus.Unidentified)
    }

    @Test
    fun whenIdentifyUserWithTokenAndPasswordWithExpiredTokenAndValidPasswordThenItShouldReturnExpired() {
        val token = "token"
        val password = Password("password")
        val passwordHash = "secret"

        val account = Account(
            emailAddress = EmailAddress("foo@bar.baz"),
            passwordHash = passwordHash,
            verificationToken = VerificationToken(
                token = token,
                expiryTimeMillis = System.currentTimeMillis() - TimeUnit.MINUTES.toMillis(5)
            )
        )

        FakeAccountRepository.setUpAccountExists(
            accountRepository,
            account
        )

        FakePasswordHashingService.setUpHash(
            passwordHashingService,
            password,
            passwordHash
        )

        val verificationStatus = deepLinkVerificationAppService.identifyUserWithTokenAndPassword(
            token,
            password.value,
            System.currentTimeMillis()
        )

        assertTrue(verificationStatus is IdentificationStatus.Expired)
    }

    @Test
    fun whenIdentifyUserWithTokenAndPasswordWithExpiredTokenAndInvalidPasswordThenItShouldReturnUnidentified() {
        val token = "token"
        val password = Password("password")
        val passwordHash = "secret"

        val wrongPassword = Password("wrong password")
        val wrongPasswordHash = "wrong secret"

        val account = Account(
            emailAddress = EmailAddress("foo@bar.baz"),
            passwordHash = passwordHash,
            verificationToken = VerificationToken(
                token = token,
                expiryTimeMillis = System.currentTimeMillis() - TimeUnit.MINUTES.toMillis(5)
            )
        )

        FakeAccountRepository.setUpAccountExists(
            accountRepository,
            account
        )

        FakePasswordHashingService.setUpHash(
            passwordHashingService,
            password,
            passwordHash
        )

        FakePasswordHashingService.setUpHash(
            passwordHashingService,
            wrongPassword,
            wrongPasswordHash
        )

        val verificationStatus = deepLinkVerificationAppService.identifyUserWithTokenAndPassword(
            token,
            wrongPassword.value,
            System.currentTimeMillis()
        )

        assertTrue(verificationStatus is IdentificationStatus.Unidentified)
    }

    @Test
    fun whenIdentifyUserWithTokenAndPasswordWithInvalidPasswordThenItShouldReturnUnidentified() {
        val token = "token"
        val password = Password("password")
        val passwordHash = "secret"

        val wrongPassword = Password("wrong password")
        val wrongPasswordHash = "wrong secret"

        val account = Account(
            emailAddress = EmailAddress("foo@bar.baz"),
            passwordHash = passwordHash,
            verificationToken = VerificationToken(
                token = token,
                expiryTimeMillis = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(5)
            )
        )

        FakeAccountRepository.setUpAccountExists(
            accountRepository,
            account
        )

        FakePasswordHashingService.setUpHash(
            passwordHashingService,
            password,
            passwordHash
        )

        FakePasswordHashingService.setUpHash(
            passwordHashingService,
            wrongPassword,
            wrongPasswordHash
        )

        val identificationStatus = deepLinkVerificationAppService.identifyUserWithTokenAndPassword(
            token,
            wrongPassword.value,
            System.currentTimeMillis()
        )

        assertTrue(identificationStatus is IdentificationStatus.Unidentified)
    }

}