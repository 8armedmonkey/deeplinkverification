package sys.deeplinkverification.domain

import com.nhaarman.mockitokotlin2.whenever
import sys.deeplinkverification.domain.VerificationToken
import sys.deeplinkverification.domain.VerificationTokenService

object FakeVerificationTokenService {

    fun setUpGenerateNewToken(
        verificationTokenService: VerificationTokenService,
        newVerificationToken: VerificationToken
    ) {
        whenever(verificationTokenService.generateNewToken()).thenReturn(newVerificationToken)
    }

}