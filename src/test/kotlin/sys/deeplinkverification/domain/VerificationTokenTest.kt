package sys.deeplinkverification.domain

import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import sys.deeplinkverification.domain.VerificationToken
import java.util.concurrent.TimeUnit

class VerificationTokenTest {

    @Test
    fun whenCurrentTimeMillisIsLessThanExpiryTimeMillisThenIsExpiredShouldBeFalse() {
        val currentTimeMillis = System.currentTimeMillis()

        val verificationToken = VerificationToken(
            "token",
            currentTimeMillis + TimeUnit.MINUTES.toMillis(5)
        )

        assertFalse(verificationToken.isExpired(currentTimeMillis))
    }

    @Test
    fun whenCurrentTimeMillisIsEqualToExpiryTimeMillisThenIsExpiredShouldBeTrue() {
        val currentTimeMillis = System.currentTimeMillis()

        val verificationToken = VerificationToken(
            "token",
            currentTimeMillis
        )

        assertTrue(verificationToken.isExpired(currentTimeMillis))
    }

    @Test
    fun whenCurrentTimeMillisIsGreaterThanExpiryTimeMillisThenIsExpiredShouldBeTrue() {
        val currentTimeMillis = System.currentTimeMillis()

        val verificationToken = VerificationToken(
            "token",
            currentTimeMillis - TimeUnit.MINUTES.toMillis(5)
        )

        assertTrue(verificationToken.isExpired(currentTimeMillis))
    }

}