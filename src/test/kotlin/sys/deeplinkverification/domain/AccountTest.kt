package sys.deeplinkverification.domain

import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import java.util.*
import java.util.concurrent.TimeUnit

class AccountTest {

    private lateinit var account: Account

    @Before
    fun setUp() {
        account = Account(
            id = UUID.randomUUID(),
            emailAddress = EmailAddress("foo@bar.baz"),
            passwordHash = "foo"
        )
    }

    @Test
    fun whenChangePasswordThenItShouldChangePasswordHash() {
        account.changePassword("bar")

        assertTrue(account.verifyPassword("foo") is VerificationStatus.Invalid)
        assertTrue(account.verifyPassword("bar") is VerificationStatus.Valid)
    }

    @Test
    fun whenVerifyPasswordAndPasswordHashIsSameThenItShouldReturnStatusValid() {
        val verificationStatus = account.verifyPassword("foo")

        assertTrue(verificationStatus is VerificationStatus.Valid)
    }

    @Test
    fun whenVerifyPasswordAndPasswordHashIsDifferentThenItShouldReturnStatusInvalid() {
        val verificationStatus = account.verifyPassword("bar")

        assertTrue(verificationStatus is VerificationStatus.Invalid)
        assertTrue(
            (verificationStatus as VerificationStatus.Invalid).reason
                    is PasswordVerificationInvalidReason.Different
        )
    }

    @Test
    fun whenChangeVerificationTokenThenItShouldChangeVerificationToken() {
        account.changeVerificationToken(
            VerificationToken(
                token = "token",
                expiryTimeMillis = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(5)
            )
        )

        account.changeVerificationToken(
            VerificationToken(
                token = "newToken",
                expiryTimeMillis = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(5)
            )
        )

        assertTrue(
            account.verifyToken("token", System.currentTimeMillis())
                    is VerificationStatus.Invalid
        )

        assertTrue(
            account.verifyToken("newToken", System.currentTimeMillis())
                    is VerificationStatus.Valid
        )
    }

    @Test
    fun whenVerifyTokenAndTokenIsValidThenItShouldReturnStatusValid() {
        account.changeVerificationToken(
            VerificationToken(
                token = "token",
                expiryTimeMillis = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(5)
            )
        )

        val verificationStatus = account.verifyToken("token", System.currentTimeMillis())

        assertTrue(verificationStatus is VerificationStatus.Valid)
    }

    @Test
    fun whenVerifyTokenAndTokenHasNotBeenSetThenItShouldReturnStatusInvalid() {
        val verificationStatus = account.verifyToken("token", System.currentTimeMillis())

        assertTrue(verificationStatus is VerificationStatus.Invalid)
        assertTrue(
            (verificationStatus as VerificationStatus.Invalid).reason
                    is TokenVerificationInvalidReason.NotSetYet
        )
    }

    @Test
    fun whenVerifyTokenAndTokenIsInvalidThenItShouldReturnStatusInvalid() {
        account.changeVerificationToken(
            VerificationToken(
                token = "token",
                expiryTimeMillis = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(5)
            )
        )

        val verificationStatus = account.verifyToken("test", System.currentTimeMillis())

        assertTrue(verificationStatus is VerificationStatus.Invalid)
        assertTrue(
            (verificationStatus as VerificationStatus.Invalid).reason
                    is TokenVerificationInvalidReason.Different
        )
    }

    @Test
    fun whenVerifyTokenAndTokenIsExpiredThenItShouldReturnStatusInvalid() {
        account.changeVerificationToken(
            VerificationToken(
                token = "token",
                expiryTimeMillis = System.currentTimeMillis() - TimeUnit.MINUTES.toMillis(5)
            )
        )

        val verificationStatus = account.verifyToken("token", System.currentTimeMillis())

        assertTrue(verificationStatus is VerificationStatus.Invalid)
        assertTrue(
            (verificationStatus as VerificationStatus.Invalid).reason
                    is TokenVerificationInvalidReason.Expired
        )
    }

    @Test
    fun whenIdentifyWithValidTokenAndPasswordThenItShouldReturnValid() {
        account.changeVerificationToken(
            VerificationToken(
                token = "token",
                expiryTimeMillis = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(5)
            )
        )

        val verificationStatus =
            account.verifyAndConsumeValidToken("token", "foo", System.currentTimeMillis())

        assertTrue(verificationStatus is VerificationStatus.Valid)
    }

    @Test
    fun whenIdentifyWithInvalidTokenThenItShouldReturnInvalid() {
        account.changeVerificationToken(
            VerificationToken(
                token = "token",
                expiryTimeMillis = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(5)
            )
        )

        val verificationStatus =
            account.verifyAndConsumeValidToken("test", "foo", System.currentTimeMillis())

        assertTrue(verificationStatus is VerificationStatus.Invalid)
        assertTrue(
            (verificationStatus as VerificationStatus.Invalid).reason
                    is TokenVerificationInvalidReason.Different
        )
    }

    @Test
    fun whenIdentifyWithExpiredTokenAndValidPasswordThenItShouldReturnInvalid() {
        account.changeVerificationToken(
            VerificationToken(
                token = "token",
                expiryTimeMillis = System.currentTimeMillis() - TimeUnit.MINUTES.toMillis(5)
            )
        )

        val verificationStatus =
            account.verifyAndConsumeValidToken("token", "foo", System.currentTimeMillis())

        assertTrue(verificationStatus is VerificationStatus.Invalid)
        assertTrue(
            (verificationStatus as VerificationStatus.Invalid).reason
                    is TokenVerificationInvalidReason.Expired
        )
    }

    @Test
    fun whenIdentifyWithExpiredTokenAndInvalidPasswordThenItShouldReturnInvalid() {
        account.changeVerificationToken(
            VerificationToken(
                token = "token",
                expiryTimeMillis = System.currentTimeMillis() - TimeUnit.MINUTES.toMillis(5)
            )
        )

        val verificationStatus =
            account.verifyAndConsumeValidToken("token", "bar", System.currentTimeMillis())

        assertTrue(verificationStatus is VerificationStatus.Invalid)
        assertTrue(
            (verificationStatus as VerificationStatus.Invalid).reason
                    is PasswordVerificationInvalidReason.Different
        )
    }

    @Test
    fun whenIdentifyWithInvalidPasswordThenItShouldReturnInvalid() {
        account.changeVerificationToken(
            VerificationToken(
                token = "token",
                expiryTimeMillis = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(5)
            )
        )

        val verificationStatus =
            account.verifyAndConsumeValidToken("token", "bar", System.currentTimeMillis())

        assertTrue(verificationStatus is VerificationStatus.Invalid)
        assertTrue(
            (verificationStatus as VerificationStatus.Invalid).reason
                    is PasswordVerificationInvalidReason.Different
        )
    }

}