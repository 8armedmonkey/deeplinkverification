package sys.deeplinkverification.domain

import org.junit.Assert.assertEquals
import org.junit.Test
import sys.deeplinkverification.domain.DeepLinkService
import java.net.URI

class DeepLinkServiceTest {

    @Test
    fun whenCreateDeepLinkWithExistingQueryThenItShouldAttachTokenToBaseUri() {
        val deepLinkService = DeepLinkService(
            baseUri = URI.create("foo://bar/baz?p=1&q=2")
        )

        val deepLinkUri = deepLinkService.createDeepLink("test")

        assertEquals("foo://bar/baz?p=1&q=2&token=test", deepLinkUri.toString())
    }

    @Test
    fun whenCreateDeepLinkThenItShouldAttachTokenToBaseUri() {
        val deepLinkService = DeepLinkService(
            baseUri = URI.create("foo://bar/baz")
        )

        val deepLinkUri = deepLinkService.createDeepLink("test")

        assertEquals("foo://bar/baz?token=test", deepLinkUri.toString())
    }

}