package sys.deeplinkverification.domain

import org.junit.Assert.assertEquals
import org.junit.Test
import sys.deeplinkverification.domain.EmailAddress
import sys.deeplinkverification.domain.InvalidEmailAddressException

class EmailAddressTest {

    @Test
    fun whenEmailIsValidThenItShouldNotThrowException() {
        val emailAddress = EmailAddress("foo@bar.baz")

        assertEquals("foo@bar.baz", emailAddress.value)
    }

    @Test(expected = InvalidEmailAddressException::class)
    fun whenEmailIsInvalidThenItShouldThrowException() {
        EmailAddress("foo")
    }

}