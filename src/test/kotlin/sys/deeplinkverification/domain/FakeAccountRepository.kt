package sys.deeplinkverification.domain

import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.whenever
import java.util.*

object FakeAccountRepository {

    fun setUpAccountNotExists(
        accountRepository: AccountRepository,
        accountId: UUID
    ) {
        whenever(accountRepository.retrieve(eq(accountId))).thenReturn(null)
        whenever(accountRepository.contains(eq(accountId))).thenReturn(false)
    }

    fun setUpAccountNotExists(
        accountRepository: AccountRepository,
        emailAddress: EmailAddress
    ) {
        whenever(accountRepository.retrieveByEmailAddress(eq(emailAddress))).thenReturn(null)
        whenever(accountRepository.containsEmailAddress(eq(emailAddress))).thenReturn(false)
    }

    fun setUpAccountNotExists(
        accountRepository: AccountRepository,
        token: String
    ) {
        whenever(accountRepository.retrieveByToken(eq(token))).thenReturn(null)
        whenever(accountRepository.containsToken(eq(token))).thenReturn(false)
    }

    fun setUpAccountExists(accountRepository: AccountRepository, account: Account) {
        whenever(accountRepository.retrieve(eq(account.id))).thenReturn(account)
        whenever(accountRepository.contains(eq(account.id))).thenReturn(true)
        whenever(accountRepository.retrieveByEmailAddress(eq(account.emailAddress))).thenReturn(account)
        whenever(accountRepository.containsEmailAddress(eq(account.emailAddress))).thenReturn(true)

        account.verificationToken?.let {
            whenever(accountRepository.retrieveByToken(eq(it.token))).thenReturn(account)
            whenever(accountRepository.containsToken(eq(it.token))).thenReturn(true)
        }
    }

}