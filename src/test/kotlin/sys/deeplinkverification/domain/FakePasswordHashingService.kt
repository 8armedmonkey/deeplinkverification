package sys.deeplinkverification.domain

import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.whenever
import sys.deeplinkverification.domain.Password
import sys.deeplinkverification.domain.PasswordHashingService

object FakePasswordHashingService {

    fun setUpHash(
        passwordHashingService: PasswordHashingService,
        password: Password,
        passwordHash: String
    ) {
        whenever(passwordHashingService.hash(eq(password))).thenReturn(passwordHash)
    }

}