package sys.deeplinkverification.domain

import org.junit.Assert.assertEquals
import org.junit.Test
import sys.deeplinkverification.domain.InsufficientPasswordLengthException
import sys.deeplinkverification.domain.Password

class PasswordTest {

    @Test
    fun whenPasswordLengthIsSufficientThenItShouldNotThrowException() {
        val password = Password("password")

        assertEquals("password", password.value)
    }

    @Test(expected = InsufficientPasswordLengthException::class)
    fun whenPasswordLengthIsNotSufficientThenItShouldThrowException() {
        Password("pass")
    }

}