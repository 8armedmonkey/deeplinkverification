package sys.deeplinkverification.infrastructure.type

import org.junit.Assert.assertEquals
import org.junit.Test

class ByteArrayExtensionsTest {

    @Test
    fun whenConvertToHexStringThenItShouldConvertCorrectly() {
        val bytes = byteArrayOf(
            0x91.toByte(),
            0x06.toByte(),
            0x24.toByte(),
            0xD8.toByte(),
            0x87.toByte(),
            0x14.toByte(),
            0xBF.toByte(),
            0x79.toByte(),
            0x2F.toByte(),
            0xEC.toByte()
        )

        val hex = bytes.toHexString()

        assertEquals("910624D88714BF792FEC", hex)
    }

}