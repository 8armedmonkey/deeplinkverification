package sys.deeplinkverification.infrastructure.database

import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class DatabaseConnectionPoolImplTest {

    private lateinit var databaseConnectionPool: DatabaseConnectionPool

    @Before
    fun setUp() {
        databaseConnectionPool = getDatabaseConnectionPool()
        databaseConnectionPool.getConnection().use { it.dropDatabase() }
    }

    @Test
    fun whenConnectionIsReturnedThenItShouldBeReusable() {
        databaseConnectionPool.getConnection().use {
            it.createStatement().execute("CREATE TABLE test_message (content TEXT)")
            it.createStatement().execute("INSERT INTO test_message (content) VALUES ('hello'), ('world')")
        }

        databaseConnectionPool.getConnection().use {
            it.createStatement().executeQuery("SELECT * FROM test_message").use { resultSet ->
                resultSet.next()
                assertEquals("hello", resultSet.getString("content"))

                resultSet.next()
                assertEquals("world", resultSet.getString("content"))
            }
        }
    }

    @After
    fun tearDown() {
        databaseConnectionPool.getConnection().use { it.dropDatabase() }
    }

}