package sys.deeplinkverification.infrastructure.database

import org.junit.Assert.assertEquals
import org.junit.Test

class DatabaseConnectionStringTest {

    @Test
    fun whenConvertedToStringThenDatabaseConnectionStringShouldBeCorrect() {
        val databaseConnectionString = DatabaseConnectionString("h2", "repositories.h2.db")

        assertEquals("jdbc:h2:repositories.h2.db", databaseConnectionString.asJdbcConnectionString())
    }

}