package sys.deeplinkverification.infrastructure.database

import org.junit.Assert.assertEquals
import org.junit.Test
import sys.deeplinkverification.infrastructure.configuration.Configuration
import java.util.*

class ConfigurationExtensionsTest {

    @Test
    fun whenDbmsAndUrlAreSpecifiedThenTheyShouldBeUsedInConnectionString() {
        val customConnectionStringDbms = "mysql"
        val customConnectionStringUrl = "//localhost:3306/repositories"

        val configuration = Configuration(
            Properties().apply {
                setProperty(PROPERTY_CONNECTION_STRING_DBMS, customConnectionStringDbms)
                setProperty(PROPERTY_CONNECTION_STRING_URL, customConnectionStringUrl)
            }
        )

        val databaseConnectionString = configuration.getDatabaseConnectionString()

        assertEquals(customConnectionStringDbms, databaseConnectionString.dbms)
        assertEquals(customConnectionStringUrl, databaseConnectionString.url)
    }

    @Test
    fun whenBothDbmsAndUrlAreNotSpecifiedThenDefaultValuesShouldBeUsedInConnectionString() {
        val configuration = Configuration(Properties())

        val databaseConnectionString = configuration.getDatabaseConnectionString()

        assertEquals(DEFAULT_DBMS, databaseConnectionString.dbms)
        assertEquals(DEFAULT_DB_URL, databaseConnectionString.url)
    }

}