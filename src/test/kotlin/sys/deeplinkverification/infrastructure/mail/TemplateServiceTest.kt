package sys.deeplinkverification.infrastructure.mail

import org.junit.Assert.assertEquals
import org.junit.Test
import sys.deeplinkverification.infrastructure.template.TemplateServiceImpl
import java.io.StringWriter

class TemplateServiceTest {

    @Test
    fun whenWrittenThenItShouldProduceCorrectOutput() {
        val templateService = TemplateServiceImpl

        StringWriter().use { writer ->
            val firstName = "John"
            val lastName = "Doe"

            templateService.getTemplate("velocity/template/message.vm").apply {
                put("firstName", firstName)
                put("lastName", lastName)

                write(writer)
            }

            assertEquals("Hello, $firstName $lastName.", writer.toString())
        }
    }

}