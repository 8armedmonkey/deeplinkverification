package sys.deeplinkverification.infrastructure.configuration

import org.junit.Assert.assertEquals
import org.junit.Test
import sys.deeplinkverification.infrastructure.database.PROPERTY_CONNECTION_STRING_DBMS
import sys.deeplinkverification.infrastructure.database.PROPERTY_CONNECTION_STRING_URL
import sys.deeplinkverification.infrastructure.environment.Environment
import java.io.File
import java.io.FileOutputStream
import java.util.*

class ConfigurationTest {

    @Test
    fun whenConstructedFromPropertiesThenItShouldCopyAllProperties() {
        val customConnectionStringDbms = "mysql"
        val customConnectionStringUrl = "//localhost:3306/repositories"

        val properties = Properties().apply {
            setProperty(PROPERTY_CONNECTION_STRING_DBMS, customConnectionStringDbms)
            setProperty(PROPERTY_CONNECTION_STRING_URL, customConnectionStringUrl)
        }

        Configuration(properties).apply {
            assertEquals(customConnectionStringDbms, get(PROPERTY_CONNECTION_STRING_DBMS))
            assertEquals(customConnectionStringUrl, get(PROPERTY_CONNECTION_STRING_URL))
        }
    }

    @Test
    fun whenCustomConfigurationFileExistsThenConfigurationIsLoadedFromConfigurationFile() {
        val executableDirectory = Environment.executableDirectory()
        val customConnectionStringDbms = "mysql"
        val customConnectionStringUrl = "//localhost:3306/repositories"

        val customConfigurationFile = File(executableDirectory, Configuration.CONFIGURATION_FILE).also { file ->
            Properties().apply {
                setProperty(PROPERTY_CONNECTION_STRING_DBMS, customConnectionStringDbms)
                setProperty(PROPERTY_CONNECTION_STRING_URL, customConnectionStringUrl)

                FileOutputStream(file).use {
                    store(it, null)
                }
            }
        }

        Configuration.createInstance().apply {
            assertEquals(customConnectionStringDbms, get(PROPERTY_CONNECTION_STRING_DBMS))
            assertEquals(customConnectionStringUrl, get(PROPERTY_CONNECTION_STRING_URL))
        }

        customConfigurationFile.delete()
    }

    @Test
    fun whenCustomConfigurationFileNotExistsThenConfigurationIsLoadedFromResources() {
        val properties = Properties().apply {
            Environment.getResourceAsStream(Configuration.CONFIGURATION_FILE).use {
                load(it)
            }
        }

        val defaultConnectionStringDbms = properties[PROPERTY_CONNECTION_STRING_DBMS]
        val defaultConnectionStringUrl = properties[PROPERTY_CONNECTION_STRING_URL]

        Configuration.createInstance().apply {
            assertEquals(defaultConnectionStringDbms, get(PROPERTY_CONNECTION_STRING_DBMS))
            assertEquals(defaultConnectionStringUrl, get(PROPERTY_CONNECTION_STRING_URL))
        }
    }

    @Test
    fun whenFilterThenItShouldReturnAllMatchingProperties() {
        val configuration = Configuration(Properties().apply {
            put("db.connection.string.dbms", "h2")
            put("db.connection.string.url", "repositories.h2.db")
            put("mail.smtp.host", "smtp.server.com")
            put("mail.smtp.port", "587")
            put("mail.smtp.auth", "true")
            put("mail.smtp.starttls.enable", "true")
            put("verification.token.duration.millis", "3600000")
        })

        val mailSmtpProperties = configuration.filter { it.startsWith("mail.smtp") }

        assertEquals(4, mailSmtpProperties.size)
        assertEquals("smtp.server.com", mailSmtpProperties.getProperty("mail.smtp.host"))
        assertEquals("587", mailSmtpProperties.getProperty("mail.smtp.port"))
        assertEquals("true", mailSmtpProperties.getProperty("mail.smtp.auth"))
        assertEquals("true", mailSmtpProperties.getProperty("mail.smtp.starttls.enable"))
    }

    @Test
    fun whenFilterAndNoMatchingPropertiesThenItShouldReturnEmptyProperties() {
        val configuration = Configuration(Properties().apply {
            put("db.connection.string.dbms", "h2")
            put("db.connection.string.url", "repositories.h2.db")
            put("mail.smtp.host", "smtp.server.com")
            put("mail.smtp.port", "587")
            put("mail.smtp.auth", "true")
            put("mail.smtp.starttls.enable", "true")
            put("verification.token.duration.millis", "3600000")
        })

        val mailSmtpProperties = configuration.filter { it.startsWith("foo") }

        assertEquals(0, mailSmtpProperties.size)
    }

}