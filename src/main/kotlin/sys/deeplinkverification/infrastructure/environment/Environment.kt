package sys.deeplinkverification.infrastructure.environment

import java.io.File
import java.io.InputStream
import java.net.URI

object Environment {

    fun getResourceAsStream(name: String): InputStream? = javaClass.classLoader.getResourceAsStream(name)

    fun executableDirectory(): File = File(executableUri()).parentFile

    private fun executableUri(): URI = javaClass.protectionDomain.codeSource.location.toURI()

}