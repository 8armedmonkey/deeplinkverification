package sys.deeplinkverification.infrastructure.verification

import java.util.concurrent.TimeUnit

const val PROPERTY_VERIFICATION_TOKEN_DURATION_MILLIS = "verification.token.duration.millis"

val DEFAULT_VERIFICATION_TOKEN_DURATION_MILLIS = TimeUnit.HOURS.toMillis(1)