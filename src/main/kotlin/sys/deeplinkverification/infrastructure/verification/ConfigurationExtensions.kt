package sys.deeplinkverification.infrastructure.verification

import sys.deeplinkverification.infrastructure.configuration.Configuration

fun Configuration.getVerificationTokenDurationMillis(): Long {
    return get(PROPERTY_VERIFICATION_TOKEN_DURATION_MILLIS)?.toLong()
        ?: DEFAULT_VERIFICATION_TOKEN_DURATION_MILLIS
}