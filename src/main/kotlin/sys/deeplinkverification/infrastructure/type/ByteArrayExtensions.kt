package sys.deeplinkverification.infrastructure.type

fun ByteArray.toHexString(): String = joinToString("") { "%02X".format(it) }