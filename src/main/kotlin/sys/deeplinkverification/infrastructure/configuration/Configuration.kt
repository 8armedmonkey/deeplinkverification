package sys.deeplinkverification.infrastructure.configuration

import sys.deeplinkverification.infrastructure.environment.Environment
import java.io.File
import java.io.FileInputStream
import java.util.*

class Configuration(sourceProperties: Properties) {

    private val properties = Properties().apply {
        sourceProperties.stringPropertyNames().forEach { key ->
            setProperty(key, sourceProperties.getProperty(key))
        }
    }

    fun get(key: String): String? = properties.getProperty(key)

    fun contains(key: String): Boolean = properties.containsKey(key)

    fun filter(fn: (String) -> Boolean): Properties =
        properties.stringPropertyNames()
            .filter { fn(it) }
            .fold(Properties()) { filteredProperties, propertyName ->
                filteredProperties.apply {
                    put(propertyName, properties.getProperty(propertyName))
                }
            }

    companion object {

        val instance: Configuration by lazy {
            createInstance()
        }

        internal const val CONFIGURATION_FILE = "config.properties"

        internal fun createInstance(): Configuration {
            val customConfigurationFile = File(Environment.executableDirectory(), CONFIGURATION_FILE)
            val sourceProperties = Properties()

            if (customConfigurationFile.exists()) {
                FileInputStream(customConfigurationFile).use {
                    sourceProperties.load(it)
                }
            } else {
                Environment.getResourceAsStream(CONFIGURATION_FILE).use {
                    sourceProperties.load(it)
                }
            }

            return Configuration(sourceProperties)
        }

    }

}