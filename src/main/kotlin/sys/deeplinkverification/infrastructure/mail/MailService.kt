package sys.deeplinkverification.infrastructure.mail

import sys.deeplinkverification.domain.EmailAddress

interface MailService {

    fun send(
        recipients: List<EmailAddress>,
        subject: String,
        message: String
    )

}