package sys.deeplinkverification.infrastructure.mail

import sys.deeplinkverification.domain.EmailAddress
import sys.deeplinkverification.infrastructure.configuration.Configuration
import java.util.*
import javax.mail.*
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage

internal object MailServiceImpl : MailService {

    private const val MESSAGE_ENCODING = "UTF-8"

    private val properties: Properties = Configuration.instance.getMailServerProperties()

    override fun send(recipients: List<EmailAddress>, subject: String, message: String) {
        val username = properties.getProperty(PROPERTY_SMTP_MAIL_USER)
        val password = properties.getProperty(PROPERTY_SMTP_MAIL_PASSWORD)

        Transport.send(
            MimeMessage(
                Session.getInstance(properties, object : Authenticator() {
                    override fun getPasswordAuthentication(): PasswordAuthentication =
                        PasswordAuthentication(username, password)
                })
            ).apply {
                setRecipients(Message.RecipientType.TO, InternetAddress.parse(
                    recipients.joinToString(",") { it.value }
                ))
                setSubject(subject, MESSAGE_ENCODING)
                setText(message, MESSAGE_ENCODING)
            })
    }

}