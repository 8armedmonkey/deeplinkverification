package sys.deeplinkverification.infrastructure.mail

const val PROPERTY_SMTP_MAIL_USER = "smtp.mail.user"
const val PROPERTY_SMTP_MAIL_PASSWORD = "smtp.mail.password"