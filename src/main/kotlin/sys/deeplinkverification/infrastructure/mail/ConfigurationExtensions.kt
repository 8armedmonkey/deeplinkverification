package sys.deeplinkverification.infrastructure.mail

import sys.deeplinkverification.infrastructure.configuration.Configuration
import java.util.*

fun Configuration.getMailServerProperties(): Properties {
    return filter { it.startsWith("mail.smtp") }
}