package sys.deeplinkverification.infrastructure.database

import org.flywaydb.core.Flyway
import javax.sql.DataSource

fun getDatabaseConnectionPool(): DatabaseConnectionPool = DatabaseConnectionPoolImpl

fun migrateDatabase(dataSource: DataSource) {
    Flyway.configure()
        .dataSource(dataSource)
        .load()
        .migrate()
}