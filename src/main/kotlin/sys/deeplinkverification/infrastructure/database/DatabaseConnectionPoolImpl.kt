package sys.deeplinkverification.infrastructure.database

import org.apache.commons.dbcp2.BasicDataSource
import sys.deeplinkverification.infrastructure.configuration.Configuration
import java.sql.Connection
import javax.sql.DataSource

internal object DatabaseConnectionPoolImpl : DatabaseConnectionPool {

    private val dataSource = BasicDataSource().apply {
        url = Configuration.instance.getDatabaseConnectionString().asJdbcConnectionString()
    }

    override fun getDataSource(): DataSource = dataSource

    override fun getConnection(): Connection = dataSource.connection

}