package sys.deeplinkverification.infrastructure.database

import sys.deeplinkverification.infrastructure.environment.Environment
import java.io.File

const val PROPERTY_CONNECTION_STRING_DBMS = "db.connection.string.dbms"
const val PROPERTY_CONNECTION_STRING_URL = "db.connection.string.url"

const val DEFAULT_DBMS = "h2"
val DEFAULT_DB_URL: String = File(Environment.executableDirectory(), "repositories.h2.db").absolutePath