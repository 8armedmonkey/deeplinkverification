package sys.deeplinkverification.infrastructure.database

data class DatabaseConnectionString(
    val dbms: String,
    val url: String
) {

    fun asJdbcConnectionString(): String = "jdbc:$dbms:$url"

}