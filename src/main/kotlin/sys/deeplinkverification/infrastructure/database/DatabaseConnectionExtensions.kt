package sys.deeplinkverification.infrastructure.database

import java.sql.Connection

fun Connection.dropDatabase(): Int = createStatement().executeUpdate("DROP ALL OBJECTS DELETE FILES")