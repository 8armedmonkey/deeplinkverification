package sys.deeplinkverification.infrastructure.database

import java.sql.Connection
import javax.sql.DataSource

interface DatabaseConnectionPool {

    fun getDataSource(): DataSource

    fun getConnection(): Connection

}