package sys.deeplinkverification.infrastructure.database

import sys.deeplinkverification.infrastructure.configuration.Configuration

fun Configuration.getDatabaseConnectionString(): DatabaseConnectionString {
    val dbms = get(PROPERTY_CONNECTION_STRING_DBMS)
    val url = get(PROPERTY_CONNECTION_STRING_URL)

    return if (!dbms.isNullOrBlank() && !url.isNullOrBlank()) {
        DatabaseConnectionString(dbms, url)
    } else {
        DatabaseConnectionString(DEFAULT_DBMS, DEFAULT_DB_URL)
    }
}