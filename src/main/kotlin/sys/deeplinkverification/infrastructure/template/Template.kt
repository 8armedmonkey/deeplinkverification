package sys.deeplinkverification.infrastructure.template

import java.io.Writer

interface Template {

    fun put(key: String, value: Any)

    fun write(writer: Writer)

}