package sys.deeplinkverification.infrastructure.template

interface TemplateService {

    fun getTemplate(path: String): Template

}