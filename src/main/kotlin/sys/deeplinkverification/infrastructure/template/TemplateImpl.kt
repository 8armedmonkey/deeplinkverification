package sys.deeplinkverification.infrastructure.template

import org.apache.velocity.VelocityContext
import java.io.Writer

internal class TemplateImpl(
    private val template: org.apache.velocity.Template
) : Template {

    private val context = VelocityContext()

    override fun put(key: String, value: Any) {
        context.put(key, value)
    }

    override fun write(writer: Writer) {
        template.merge(context, writer)
    }

}