package sys.deeplinkverification.infrastructure.template

import org.apache.velocity.app.VelocityEngine
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader

object TemplateServiceImpl : TemplateService {

    private val engine = VelocityEngine().apply {
        setProperty(VelocityEngine.RESOURCE_LOADERS, "class,file")
        setProperty("resource.loader.class.class", ClasspathResourceLoader::class.java.name)
        init()
    }

    override fun getTemplate(path: String): Template =
        TemplateImpl(engine.getTemplate(path))

}