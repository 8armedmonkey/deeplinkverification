package sys.deeplinkverification.domain

sealed class TokenVerificationInvalidReason : VerificationStatus.InvalidReason() {

    object NotSetYet : TokenVerificationInvalidReason()

    object Different : TokenVerificationInvalidReason()

    object Expired : TokenVerificationInvalidReason()

    object NotExists : TokenVerificationInvalidReason()

}