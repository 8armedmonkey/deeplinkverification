package sys.deeplinkverification.domain

data class Password(val value: String) {

    init {
        if (value.length < MIN_LENGTH) {
            throw InsufficientPasswordLengthException(MIN_LENGTH)
        }
    }

    companion object {

        const val MIN_LENGTH = 8

    }

}