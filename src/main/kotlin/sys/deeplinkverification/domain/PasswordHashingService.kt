package sys.deeplinkverification.domain

interface PasswordHashingService {

    fun hash(password: Password): String

    fun check(password: Password, hashedPassword: String): Boolean

}