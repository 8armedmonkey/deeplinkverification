package sys.deeplinkverification.domain

import java.util.*

sealed class IdentificationStatus {

    data class Identified(val accountId: UUID) : IdentificationStatus()

    object Expired : IdentificationStatus()

    object Unidentified : IdentificationStatus()

}