package sys.deeplinkverification.domain

import java.util.*

interface AccountRepository {

    fun retrieve(id: UUID): Account?

    fun retrieveByEmailAddress(emailAddress: EmailAddress): Account?

    fun retrieveByToken(token: String): Account?

    fun contains(id: UUID): Boolean

    fun containsEmailAddress(emailAddress: EmailAddress): Boolean

    fun containsToken(token: String): Boolean

    fun store(account: Account)

}