package sys.deeplinkverification.domain

class InsufficientPasswordLengthException(
    val minimumLength: Int
) : RuntimeException()