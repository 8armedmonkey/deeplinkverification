package sys.deeplinkverification.domain

sealed class VerificationStatus {

    object Valid : VerificationStatus()

    data class Invalid(val reason: InvalidReason) : VerificationStatus()

    open class InvalidReason

}