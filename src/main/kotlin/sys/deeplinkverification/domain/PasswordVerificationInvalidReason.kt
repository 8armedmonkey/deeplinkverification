package sys.deeplinkverification.domain

sealed class PasswordVerificationInvalidReason : VerificationStatus.InvalidReason() {

    object Different : PasswordVerificationInvalidReason()

}