package sys.deeplinkverification.domain

import java.net.URI

interface DeepLinkSendingService {

    fun sendDeepLink(emailAddress: EmailAddress, deepLinkUri: URI)

}