package sys.deeplinkverification.domain

data class VerificationToken(
    val token: String,
    val expiryTimeMillis: Long
) {

    fun isExpired(currentTimeMillis: Long): Boolean =
        currentTimeMillis >= expiryTimeMillis

}