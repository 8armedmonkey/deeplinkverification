package sys.deeplinkverification.domain

interface VerificationTokenService {

    fun generateNewToken(): VerificationToken

}