package sys.deeplinkverification.domain

import java.util.*

class Account(
    val id: UUID = UUID.randomUUID(),
    val emailAddress: EmailAddress,
    internal var passwordHash: String,
    internal var verificationToken: VerificationToken? = null
) {

    fun changePassword(newPasswordHash: String) {
        passwordHash = newPasswordHash
    }

    fun verifyPassword(aPasswordHash: String): VerificationStatus =
        if (passwordHash == aPasswordHash) {
            VerificationStatus.Valid
        } else {
            VerificationStatus.Invalid(PasswordVerificationInvalidReason.Different)
        }

    fun changeVerificationToken(newVerificationToken: VerificationToken) {
        verificationToken = newVerificationToken
    }

    fun verifyToken(aToken: String, currentTimeMillis: Long): VerificationStatus =
        verificationToken?.let {
            when {
                it.isExpired(currentTimeMillis) ->
                    VerificationStatus.Invalid(TokenVerificationInvalidReason.Expired)
                it.token != aToken ->
                    VerificationStatus.Invalid(TokenVerificationInvalidReason.Different)
                else -> VerificationStatus.Valid
            }
        } ?: VerificationStatus.Invalid(TokenVerificationInvalidReason.NotSetYet)

    fun verifyAndConsumeValidToken(
        aToken: String,
        aPasswordHash: String,
        currentTimeMillis: Long
    ): VerificationStatus {
        val passwordVerificationStatus = verifyPassword(aPasswordHash)
        val tokenVerificationStatus = verifyToken(aToken, currentTimeMillis)

        return when {
            passwordVerificationStatus is VerificationStatus.Invalid -> passwordVerificationStatus
            tokenVerificationStatus is VerificationStatus.Invalid -> tokenVerificationStatus
            else -> {
                verificationToken = null
                VerificationStatus.Valid
            }
        }
    }

}