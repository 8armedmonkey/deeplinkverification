package sys.deeplinkverification.domain

class AccountNotExistsException : RuntimeException()