package sys.deeplinkverification.domain

import java.net.URI
import java.net.URLEncoder

class DeepLinkService(
    private val baseUri: URI
) {

    fun createDeepLink(token: String): URI = URI(
        baseUri.scheme,
        baseUri.host,
        baseUri.path,
        baseUri.query.let { query ->
            if (query.isNullOrEmpty()) {
                "token=${URLEncoder.encode(token, "UTF-8")}"
            } else {
                "$query&token=${URLEncoder.encode(token, "UTF-8")}"
            }
        },
        baseUri.fragment
    )

}