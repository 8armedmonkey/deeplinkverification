package sys.deeplinkverification.domain

class InvalidEmailAddressException : RuntimeException()