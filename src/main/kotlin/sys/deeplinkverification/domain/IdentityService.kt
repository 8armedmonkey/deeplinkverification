package sys.deeplinkverification.domain

import java.util.*

interface IdentityService {

    fun getCurrentAccountId(): UUID?

}