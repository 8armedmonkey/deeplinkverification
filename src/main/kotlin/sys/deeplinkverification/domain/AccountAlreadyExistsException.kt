package sys.deeplinkverification.domain

class AccountAlreadyExistsException : RuntimeException()