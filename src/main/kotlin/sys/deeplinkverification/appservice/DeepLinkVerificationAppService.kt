package sys.deeplinkverification.appservice

import sys.deeplinkverification.domain.*
import java.util.*

class DeepLinkVerificationAppService(
    private val accountRepository: AccountRepository,
    private val passwordHashingService: PasswordHashingService,
    private val deepLinkService: DeepLinkService,
    private val deepLinkSendingService: DeepLinkSendingService,
    private val verificationTokenService: VerificationTokenService
) {

    fun register(anEmailAddress: String, aPassword: String): UUID {
        val emailAddress = EmailAddress(anEmailAddress)
        val password = Password(aPassword)

        if (accountRepository.containsEmailAddress(emailAddress)) {
            throw AccountAlreadyExistsException()
        }

        val account = Account(
            emailAddress = emailAddress,
            passwordHash = passwordHashingService.hash(password)
        )

        accountRepository.store(account)

        return account.id
    }

    fun requestNewVerification(accountId: UUID) {
        val account = accountRepository.retrieve(accountId)
            ?: throw AccountNotExistsException()

        val newVerificationToken = verificationTokenService.generateNewToken()

        account.changeVerificationToken(newVerificationToken)

        accountRepository.store(account)

        deepLinkSendingService.sendDeepLink(
            account.emailAddress,
            deepLinkService.createDeepLink(newVerificationToken.token)
        )
    }

    fun requestNewVerification(anEmailAddress: String) {
        val emailAddress = EmailAddress(anEmailAddress)
        val account = accountRepository.retrieveByEmailAddress(emailAddress)
            ?: throw AccountNotExistsException()

        requestNewVerification(account.id)
    }

    fun verifyToken(aToken: String, currentTimeMillis: Long): VerificationStatus =
        accountRepository.retrieveByToken(aToken)?.verifyToken(
            aToken,
            currentTimeMillis
        ) ?: VerificationStatus.Invalid(TokenVerificationInvalidReason.NotExists)

    fun identifyUserWithTokenAndPassword(
        aToken: String,
        aPassword: String,
        currentTimeMillis: Long
    ): IdentificationStatus = accountRepository.retrieveByToken(aToken)?.let { account ->
        val password = Password(aPassword)
        val verificationStatus = account.verifyAndConsumeValidToken(
            aToken,
            passwordHashingService.hash(password),
            currentTimeMillis
        )

        accountRepository.store(account)

        when (verificationStatus) {
            is VerificationStatus.Invalid -> {
                if (verificationStatus.reason is TokenVerificationInvalidReason.Expired) {
                    IdentificationStatus.Expired
                } else {
                    IdentificationStatus.Unidentified
                }
            }
            else -> IdentificationStatus.Identified(account.id)
        }
    } ?: IdentificationStatus.Unidentified

}