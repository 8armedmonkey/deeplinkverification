package sys.deeplinkverification.internal

const val DEEP_LINK_MAIL_TEMPLATE_PATH = "velocity/template/deep-link-mail.vm"
const val DEEP_LINK_MAIL_SUBJECT = "Verification Link"
const val DEEP_LINK_MAIL_FIELD_VERIFICATION_HTML_LINK = "verificationHtmlLink"
const val DEEP_LINK_MAIL_FIELD_VERIFICATION_LINK = "verificationLink"