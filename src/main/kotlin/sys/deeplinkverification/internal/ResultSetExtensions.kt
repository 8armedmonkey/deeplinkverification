package sys.deeplinkverification.internal

import sys.deeplinkverification.domain.Account
import sys.deeplinkverification.domain.EmailAddress
import sys.deeplinkverification.domain.VerificationToken
import java.sql.ResultSet
import java.util.*

fun ResultSet.toAccount(): Account? {
    return Account(
        id = UUID.fromString(getString("id")),
        emailAddress = EmailAddress(getString("email_address")),
        passwordHash = getString("password_hash"),
        verificationToken = toVerificationToken()
    )
}

fun ResultSet.toVerificationToken(): VerificationToken? {
    val token = getString("token")
    val expiryTimeMillis = getLong("token_expiry_time_millis")

    return if (!token.isNullOrBlank() && expiryTimeMillis > 0) {
        VerificationToken(
            token = token,
            expiryTimeMillis = expiryTimeMillis
        )
    } else null
}