package sys.deeplinkverification.internal

import org.apache.commons.text.StringEscapeUtils
import sys.deeplinkverification.domain.DeepLinkSendingService
import sys.deeplinkverification.domain.EmailAddress
import sys.deeplinkverification.infrastructure.mail.MailService
import sys.deeplinkverification.infrastructure.template.TemplateService
import java.io.StringWriter
import java.net.URI

internal class DeepLinkSendingServiceImpl(
    private val mailService: MailService,
    private val templateService: TemplateService
) : DeepLinkSendingService {

    override fun sendDeepLink(emailAddress: EmailAddress, deepLinkUri: URI) {
        StringWriter().use { writer ->
            templateService.getTemplate(DEEP_LINK_MAIL_TEMPLATE_PATH).apply {
                put(
                    DEEP_LINK_MAIL_FIELD_VERIFICATION_HTML_LINK,
                    StringEscapeUtils.escapeHtml4(deepLinkUri.toString())
                )

                put(
                    DEEP_LINK_MAIL_FIELD_VERIFICATION_LINK,
                    deepLinkUri.toString()
                )

                write(writer)
            }

            mailService.send(listOf(emailAddress), DEEP_LINK_MAIL_SUBJECT, writer.toString())
        }
    }

}