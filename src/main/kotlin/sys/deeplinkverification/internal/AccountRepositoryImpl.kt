package sys.deeplinkverification.internal

import sys.deeplinkverification.domain.Account
import sys.deeplinkverification.domain.AccountRepository
import sys.deeplinkverification.domain.EmailAddress
import sys.deeplinkverification.infrastructure.database.DatabaseConnectionPool
import java.sql.Types
import java.util.*

internal class AccountRepositoryImpl(
    private val databaseConnectionPool: DatabaseConnectionPool
) : AccountRepository {

    override fun retrieve(id: UUID): Account? {
        return databaseConnectionPool.getConnection().use { connection ->
            connection.prepareStatement("SELECT * FROM account WHERE id = ?").apply {
                setString(1, id.toString())
            }.executeQuery().use { resultSet ->
                if (resultSet.next()) {
                    resultSet.toAccount()
                } else {
                    null
                }
            }
        }
    }

    override fun retrieveByEmailAddress(emailAddress: EmailAddress): Account? {
        return databaseConnectionPool.getConnection().use { connection ->
            connection.prepareStatement("SELECT * FROM account WHERE email_address = ?").apply {
                setString(1, emailAddress.value)
            }.executeQuery().use { resultSet ->
                if (resultSet.next()) {
                    resultSet.toAccount()
                } else {
                    null
                }
            }
        }
    }

    override fun retrieveByToken(token: String): Account? {
        return databaseConnectionPool.getConnection().use { connection ->
            connection.prepareStatement("SELECT * FROM account WHERE token = ?").apply {
                setString(1, token)
            }.executeQuery().use { resultSet ->
                if (resultSet.next()) {
                    resultSet.toAccount()
                } else {
                    null
                }
            }
        }
    }

    override fun contains(id: UUID): Boolean {
        return databaseConnectionPool.getConnection().use { connection ->
            connection.prepareStatement("SELECT COUNT(*) AS size FROM account WHERE id = ?").apply {
                setString(1, id.toString())
            }.executeQuery().use { resultSet ->
                if (resultSet.next()) {
                    resultSet.getInt("size") > 0
                } else {
                    false
                }
            }
        }
    }

    override fun containsEmailAddress(emailAddress: EmailAddress): Boolean {
        return databaseConnectionPool.getConnection().use { connection ->
            connection.prepareStatement("SELECT COUNT(*) AS size FROM account WHERE email_address = ?").apply {
                setString(1, emailAddress.value)
            }.executeQuery().use { resultSet ->
                if (resultSet.next()) {
                    resultSet.getInt("size") > 0
                } else {
                    false
                }
            }
        }
    }

    override fun containsToken(token: String): Boolean {
        return databaseConnectionPool.getConnection().use { connection ->
            connection.prepareStatement("SELECT COUNT(*) AS size FROM account WHERE token = ?").apply {
                setString(1, token)
            }.executeQuery().use { resultSet ->
                if (resultSet.next()) {
                    resultSet.getInt("size") > 0
                } else {
                    false
                }
            }
        }
    }

    override fun store(account: Account) {
        databaseConnectionPool.getConnection().use { connection ->
            connection.prepareStatement(
                "INSERT INTO account (id, email_address, password_hash, token, token_expiry_time_millis) " +
                        "VALUES (?, ?, ?, ?, ?)"
            ).apply {
                setString(1, account.id.toString())
                setString(2, account.emailAddress.value)
                setString(3, account.passwordHash)

                val verificationToken = account.verificationToken

                if (verificationToken != null) {
                    setString(4, verificationToken.token)
                    setLong(5, verificationToken.expiryTimeMillis)
                } else {
                    setNull(4, Types.VARCHAR)
                    setNull(5, Types.BIGINT)

                }
            }.execute()
        }
    }

}