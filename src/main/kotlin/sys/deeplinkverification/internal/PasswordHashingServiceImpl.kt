package sys.deeplinkverification.internal

import com.lambdaworks.crypto.SCryptUtil
import sys.deeplinkverification.domain.Password
import sys.deeplinkverification.domain.PasswordHashingService

internal class PasswordHashingServiceImpl : PasswordHashingService {

    override fun hash(password: Password): String {
        return SCryptUtil.scrypt(password.value, SCRYPT_N, SCRYPT_r, SCRYPT_p)
    }

    override fun check(password: Password, hashedPassword: String): Boolean {
        return SCryptUtil.check(password.value, hashedPassword)
    }

    // The values are picked based on:
    // * https://crypto.stackexchange.com/a/37088
    // * https://github.com/wg/scrypt
    companion object {

        private const val SCRYPT_N = 16384
        private const val SCRYPT_r = 8
        private const val SCRYPT_p = 1

    }

}