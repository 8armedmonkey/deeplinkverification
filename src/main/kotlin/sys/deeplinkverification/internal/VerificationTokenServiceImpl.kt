package sys.deeplinkverification.internal

import sys.deeplinkverification.domain.VerificationToken
import sys.deeplinkverification.domain.VerificationTokenService
import sys.deeplinkverification.infrastructure.type.toHexString
import java.security.SecureRandom

internal class VerificationTokenServiceImpl(
    private val verificationTokenDurationMillis: Long
) : VerificationTokenService {

    override fun generateNewToken(): VerificationToken {
        val bytes = ByteArray(TOKEN_SIZE_IN_BYTES)

        SecureRandom.getInstanceStrong().apply {
            setSeed(System.currentTimeMillis())
            nextBytes(bytes)
        }

        return VerificationToken(
            bytes.toHexString(),
            System.currentTimeMillis() + verificationTokenDurationMillis
        )
    }

    companion object {

        private const val TOKEN_SIZE_IN_BYTES = 32

    }

}