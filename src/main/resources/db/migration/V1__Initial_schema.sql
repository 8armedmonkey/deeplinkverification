CREATE TABLE account (
    id VARCHAR(255) NOT NULL UNIQUE,
    email_address VARCHAR(255) NOT NULL UNIQUE,
    password_hash VARCHAR(255) NOT NULL,
    token TEXT DEFAULT NULL,
    token_expiry_time_millis BIGINT DEFAULT NULL
)